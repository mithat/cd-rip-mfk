# Makefile for cd-rip-mfk

COMMANDS = cd-rip-mfk\
           flac2mp3\
           flac2mp3-gui\
           wav2mp3-mfk

DESKTOP_FILES = cd-rip-mfk.desktop

PREFIX=/usr/local
BINDIR=${PREFIX}/bin
DESKTOP_FILES_DIR=${PREFIX}/share/applications


all:
	@echo "Nothing to be done."	

clean:
	@echo "Nothing to be done."

install:
	@if [ ! -d ${PREFIX} ]; then echo "No such dir: ${PREFIX}";exit 1;fi
	@if [ ! -d ${BINDIR} ]; then mkdir -p ${BINDIR};echo "Creating ${BINDIR}";fi
	cp -f ${COMMANDS} ${BINDIR}
	@if [ ! -d ${DESKTOP_FILES_DIR} ]; then mkdir -p ${DESKTOP_FILES_DIR};echo "Creating ${DESKTOP_FILES_DIR}";fi
	cp -f ${DESKTOP_FILES} ${DESKTOP_FILES_DIR}
	@echo
	@echo "cd-rip-mfk has been installed."	

uninstall:
	@if [ ! -d ${DESKTOP_FILES_DIR} ]; then echo "No such dir: ${DESKTOP_FILES_DIR}. Nothing to do.";else cd ${DESKTOP_FILES_DIR};rm -f ${DESKTOP_FILES};fi
	@if [ ! -d ${BINDIR} ]; then echo "No such dir: ${BINDIR}. Nothing to do.";exit1;fi
	cd ${BINDIR};rm -f ${COMMANDS}
	@echo
	@echo "cd-rip-mfk has have been uninstalled."	
