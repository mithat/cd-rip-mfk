CD Rip MFK
==========

These are some brain-dead simple apps for (1) ripping CDs and (2) convering
flac files to mp3. I wrote them for my own joy. If you like them, then that
makes for even more joy.

To make this go, you'll want to install abcde, id3v2, zenity, xterm,
gnome-icon-theme, lame, and one of gedit, geany, leafpad, mousepad, kate, or medit. And
probably some others I may have missed.

All files are Copyright (C) 2010-2015 Mithat Konar and released under the GPL3,
except for flac2mp3--which was written by "Rechosen" and released without
specifying the license. I am thus assuming it's in the public domain.
