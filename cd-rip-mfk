#!/bin/bash

#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

# Copyright (c) 2010-2015 Mithat Konar

# Prompts user for Audio CD ripping action

#=== CONSTANTS =========================================================

# Menu entries
RIP_CD="Rip with abcde"
PLAY_CD="Play the CD"

EDIT_CFG="* edit a settings file *"
NEW_CFG="* create a new settings file *"

CHOOSE_CFG="Choose a configuration file"
NEW_CFG_NAME="Create a new configuration file"

# Prompts and other GUI strings
MAIN_TITLE="CD Rip MFK"
MAIN_PROMPT="What would you like to do?"
OPTIONS="Options"

RIP_TITLE="Rip CD"
RIP_PROMPT="Choose a rip setting or other option."
CONFIG_OPTIONS="Options"

CMD_NOT_FOUND="Command not found"

PROCESS_COMPLETE="Process complete."
PRESS_ANY_KEY_TO_CONTINUE="Press any key to continue..."

RIP_COMPLETE_TITLE="Ripping complete"
RIP_COMPLETE="Ripping process completed with code $?"

# Misc
CONFIGS_DIR=$HOME/.abcde-configs
THE_ICON=/usr/share/icons/gnome/16x16/devices/drive-optical.png
STD_HEIGHT=440
STD_WIDTH=660

#=== FUNCTIONS =========================================================

# Start a rip using config file $1 if found.
function rip-it()
{
	# make a list of config files found and add options for new and editing		
	#~ IFS=$'\n'
	confFiles=
	theFiles=`ls ${CONFIGS_DIR}/*.conf`
	for file in ${theFiles}
	do
		confFiles="$confFiles `basename $file .conf`"
	done
	#~ unset IFS

	action=`zenity --list \
	--height=$STD_HEIGHT \
	--width=$STD_WIDTH \
	--window-icon "$THE_ICON" \
	--title "$RIP_TITLE" \
	--text "$RIP_PROMPT" \
	--column "$CONFIG_OPTIONS" $confFiles "$EDIT_CFG" "$NEW_CFG"`
	
	if [[ $? == 1 ]] ; then
		echo "User cancelled" 1>&2
		return 1
	fi
	
	case "$action" in
		"$EDIT_CFG" )
			edit-config && rip-it
			;;
		"$NEW_CFG" )
			new-config && rip-it
			;;
		* )
			cfgfile="${CONFIGS_DIR}/${action}.conf"
			xterm -geometry 120x40 -sb -rightbar -si +sk -T "CD Ripper (abcde)" -name "CD Ripper (abcde)" -e bash -c "abcde -c \"$cfgfile\" ; echo ; read -sn 1 -p \"$PRESS_ANY_KEY_TO_CONTINUE\""
			exit 0
			;;
	esac
}

# Prompt user for config file to edit and open it in a text editor.
function edit-config()
{
	find-editor
    # TODO: trap editor not found

	cfgfile=`zenity --file-selection \
	--window-icon "$THE_ICON" \
	--title "$CHOOSE_CFG" \
	--filename="${CONFIGS_DIR}/"`

	if [[ $? == 1 ]] ; then
		echo "User cancelled" 1>&2
		return 1
	fi

    echo "cfgfile: $cfgfile"
    $theEditor "$cfgfile"
}

# Prompt user for new config file and open it in a text editor.
function new-config()
{
    find-editor
    # TODO: trap editor not found

	cfgfile=`zenity --file-selection \
	--window-icon "$THE_ICON" \
	--title "$NEW_CFG_NAME" \
	--confirm-overwrite \
	--save \
	--filename="${CONFIGS_DIR}/new.conf"`

	if [[ $? == 1 ]] ; then
		echo "User cancelled" 1>&2
		return 1
	fi
	
	cp /etc/abcde.conf "$cfgfile"
	$theEditor "$cfgfile"
}

# Find a valid editor and return it in the global $theEditor.
function find-editor
{
    if which geany > /dev/null ; then
        theEditor=geany
    elif which kate > /dev/null ; then
        theEditor=kate
    elif which gedit > /dev/null ; then
        theEditor=gedit
    elif which medit > /dev/null ; then
        theEditor=medit
    elif which mousepad > /dev/null ; then
        theEditor=mousepad
    elif which leafpad > /dev/null ; then
        theEditor=leafpad
    else
        return 1
    fi
}

#=== MAIN ==============================================================

#cd ~

# "Just rip it" version
# ---------------------
rip-it


## "Prompt for action" version
## -------------------------
## loop till we're done
#while [[ "a" == "a" ]] 
#do
#	action=`zenity --list \
#	--height=$STD_HEIGHT \
#	--width=$STD_WIDTH \
#	--window-icon "$THE_ICON" \
#	--title "$MAIN_TITLE" \
#	--text "$MAIN_PROMPT" \
#	--column "$OPTIONS" \
#		"$RIP_CD" \
#		"$PLAY_CD" \
#	`
#
#	if [[ $? == 1 ]] ; then
#		echo "User cancelled" 1>&2
#		exit
#	fi
#
#	case "$action" in
#		"$RIP_CD" )
#			rip-it
#			;;
#		"$PLAY_CD" )
#			#exaile --play-cd &
#			#vlc cdda:// &
#			audacious cdda:// &
#			exit 0
#			;;
#		* )
#			echo "command not found: $action" 1>&2
#			zenity --error --title "$CMD_NOT_FOUND" --text "\"$action\""
#			exit 1
#			;;
#	esac
#done
